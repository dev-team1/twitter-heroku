let validInputs = 0; // to count how many inputs are valid
let validation = document.getElementById('submit-button');
let validationLogin = document.getElementById('login-button');
let validationUpdate = document.getElementById('update-inform');

function invalidFunc(invalid, input) {
    // when the input is invalid
    invalid.style.display = 'inline-block';
    input.style.border = '2px solid #f0695a';
}

function validFunc(invalid, input) {
    // when the input is valid
    invalid.style.display = 'none';
    input.style.border = 'none';
    input.style.borderBottom = '2px solid #ced4da';
    validInputs++;
}

function validatePassword() {
    let input = document.getElementById('pass');
    let invalid = document.getElementById('invalid-password');
    if (
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_.])[A-Za-z\d@$!%*?&_.]{8,}$/.test(
            input.value
        ) == false
    ) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}

function validatePasswordRepeat() {
    let pass = document.getElementById('pass');
    let input = document.getElementById('pass-repeat');
    let invalid = document.getElementById('invalid-password-repeat');
    if (input.value != pass.value) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}

function validateEmail() {
    let input = document.getElementById('email');
    let str = input.value;
    let invalid = document.getElementById('invalid-email');
    if (
        /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(str) ===
        false
    ) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}

function validateDate() {
    let input = document.getElementById('date');
    let str = input.value;
    let invalid = document.getElementById('invalid-date');
    if (/\d{4}-\d{1,2}-\d{1,2}/.test(str) === false) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}

function validateFirstName() {
    let input = document.getElementById('firstName');
    let invalid = document.getElementById('invalid-first-name');
    const reg = /[a-zA-Z]{3,30}/g;
    if (reg.test(input.value) != true) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}

function validateLastName() {
    let input = document.getElementById('lastName');
    let invalid = document.getElementById('invalid-last-name');
    const reg = /[a-zA-Z]{3,30}/g;
    if (reg.test(input.value) != true) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}

function validateUsername() {
    let input = document.getElementById('username');
    let invalid = document.getElementById('invalid-username');
    const reg = /[a-zA-Z0-9]{3,15}/g;
    if (reg.test(input.value) != true) {
        invalidFunc(invalid, input); // make it invalid when regex is failed
    } else {
        validFunc(invalid, input); // make it valid when regex is fine
    }
}



if(validationUpdate != null){
    validationUpdate.onclick = function () {
        //inform.html validation
        validInputs = 0;
        validateFirstName(); // checking all inputs
        validateEmail();
        validateLastName();
        validateUsername();
        validateDate();
        if (validInputs == 4) {
            window.location.href = '../html/inform.html';
        } else {
            let formControls = document.querySelectorAll('.form-control'); // after button is clicked validate on every change of input
            for (const formControl of formControls) {
                formControl.addEventListener('change', function (event) {
                    validateFirstName(); // checking all inputs
                    validateEmail();
                    validateLastName();
                    validateUsername();
                    validateDate();
                });
            }
        }
    }
}

if(validationLogin != null) { // validation on login page
    validationLogin.onclick = function () {
        validInputs = 0;
        validateEmail();
        validatePassword();
        if (validInputs == 2) {
            // if all inputs are fine go to profile page
            window.location.href = '../html/profile.html';
        } else {
            let formControls = document.querySelectorAll('.form-control'); // after button is clicked validate on every change of input
            for (const formControl of formControls) {
                formControl.addEventListener('change', function (event) {
                    validateEmail();
                    validatePassword();
                });
            }
        }
    }
}

if(validation != null) {  // validation on register page
    validation.onclick = function () {
        //on submit button clicked
        validInputs = 0;
        validateFirstName(); // checking all inputs
        validateEmail();
        validateLastName();
        validateUsername();
        validateDate();
        validatePassword();
        validatePasswordRepeat();
        if (validInputs == 7) {
            // if all inputs are fine go to profile page
            window.location.href = '../html/profile.html';
        } else {
            let formControls = document.querySelectorAll('.form-control'); // after button is clicked validate on every change of input
            for (const formControl of formControls) {
                formControl.addEventListener('change', function (event) {
                    validateFirstName();
                    validateEmail();
                    validateLastName();
                    validateUsername();
                    validateDate();
                    validatePassword();
                    validatePasswordRepeat();
                });
            }
        }
    }
}
