let createBtn = document.getElementById('add-tweet');
let formTweet = document.querySelectorAll('.tweet-form');
let roots = document.getElementById('root');
let charCount = document.querySelectorAll('.character-counter');
let tweetForm = document.getElementById('tweet-form');
let firstName = document.querySelectorAll('.tweet-form');
let time = new Date();

function tweet() {
  if (formTweet[0].value === '') {
    alert('Write text!');
  } else {
    const newTable = document.createElement('div');
    newTable.className =
      'flex-tweet row container col-lg-9 border-dar tweet mt-3 pb-2';
    roots.insertBefore(newTable, roots.firstChild);

    const FirstName = document.createElement('div');
    FirstName.className = 'h2 text-dark font-weight-normal pt-3';
    FirstName.innerHTML = document.querySelectorAll('.tweet-form')[0].value;
    newTable.appendChild(FirstName);

    const newId = document.createElement('div');
    newId.className = 'h2 text-dark font-weight-normal pt-3';
    newId.innerHTML = document.querySelectorAll('.tweet-form')[0].value;
    newTable.appendChild(newId);

    const timeTweet = document.createElement('div');
    timeTweet.className = 'pt-3';
    timeTweet.innerHTML = time.getHours() + ':' + time.getMinutes();
    newTable.insertBefore(timeTweet, newTable.firstChild);

    const btn = document.createElement('div');
    btn.className = 'btn__new-tweet';
    newTable.appendChild(btn);

    const like = document.createElement('button');
    like.className = 'fas fa-heart flip-icon pr-2 p icon btn';
    btn.appendChild(like);

    const del = document.createElement('button');
    del.className = 'fas fa-trash-alt pr-2 icon btn';
    btn.appendChild(del);

    formTweet[0].value = '';
  }
}

//characterCount
$('textarea').keyup(function() {
    
  var characterCount = $(this).val().length,
      current = $('#current'),
      maximum = $('#maximum'),
      tweetForm = $('#tweet-form')
    
  current.text(characterCount); 
  if (characterCount == 280) {
    maximum.css('color', '#8f0001');
    current.css('color', '#8f0001');
    tweetForm.css('border-color', '#8f0001')
  }
});

// sidebar function
$(document).ready(function () {

  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

  });

createBtn.addEventListener('click', tweet);
